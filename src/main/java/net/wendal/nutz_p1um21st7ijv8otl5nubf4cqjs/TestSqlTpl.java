package net.wendal.nutz_p1um21st7ijv8otl5nubf4cqjs;

import org.nutz.dao.Sqls;
import org.nutz.dao.impl.FileSqlManager;
import org.nutz.dao.sql.Sql;
import org.nutz.plugins.sqltpl.impl.beetl.BeetlSqlTpl;

public class TestSqlTpl {

    public static void main(String[] args) {
        Sqls.setSqlBorning(BeetlSqlTpl.class);
        FileSqlManager sqls = new FileSqlManager("sqls/");
        Sql sql ;
        // 不带任何参数的时候输出什么呢?
        sql = sqls.create("select.something");
        System.out.println(sql.toPreparedStatement());
        System.out.println("================================");
        // 带一个参数呢?
        sql = sqls.create("select.something");
        sql.setParam("_name", "wendal");
        System.out.println(sql.toPreparedStatement());
        

        // 带2个参数呢?
        sql = sqls.create("select.something");
        sql.setParam("_name", "wendal");
        sql.setParam("isup", "1");
        System.out.println(sql.toPreparedStatement());
    }

}
