/* select.something */
select distinct con.item_ as x_, con.days as y_, con.isup as isup from fct as con
<% if (params.~size > 0) {%>
 WHERE 1=1
	<% if (_name! != null) {%> and con._name LIKE @_name <% } %>
	<% if (cat_! != null) { %> and con.cat_ LIKE @cat_ <% } %>
	<% if (commodity_! != null) {%> and con.commodity_ LIKE @commodity_ <% }%>
	<% if (isup! != null) {%> and con.isup = @isup <% }%>
<%} %>
ORDER BY days DESC LIMIT 0,10